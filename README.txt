Implements simple challenge-response authentication. Greatly reduces susceptiblity to replay attacks. Hopefully, with further development, will be able to eliminate the possibility of compromise through replay attacks.

This is NOT an alternative to SSL.

    * Read about challenge-response here: http://en.wikipedia.org/wiki/Challenge_response
    * Read about replay attacks with Bob and Alice here: http://en.wikipedia.org/wiki/Replay_attack

NOTE: This module requires the application of a patch to user.module!

--------------------------------------------------------------------------------


